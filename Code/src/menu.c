/**
 * @brief      Implémentation des fonctions de gestion du menu
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "menu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#include "string.h"
#include "color.h"


#define ERROR_SLEEP 500000


/**
 * @brief      Fonction pour générer un menu dynamique
 *
 * @param[in]  rows           Le nombre de lignes
 * @param      text           Le tableau des items à afficher
 * @param      title          Le titre
 * @param[in]  paddingUp      Remplissage haut
 * @param[in]  paddingBottom  Remplissage bas
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 *
 * @return     L'erreur
 */
error_t menuCreate(int rows, const char text[][MAX_STRING_SIZE],const char *title,int paddingUp,int paddingBottom,int paddingLeft,int paddingRight){
	error_t error = OK;
	char tabChoice[36] ={'1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	int reservedSpace;
	int strlenText;

	if(rows < 1) error=NB_ROWS_INVALID;
	if(rows < 1) error=NB_SPACE_INVALID;
	if(paddingUp < 0) error=PADDING_UP_INVALID;
	if(paddingBottom < 0) error=PADDING_DOWN_INVALID;
	if(paddingLeft < 0) error=PADDING_LEFT_INVALID;
	if(paddingRight < 0) error=PADDING_RIGHT_INVALID;
	if(text==NULL) error=NULL_POINTER;
	if(title==NULL) error=NULL_POINTER;

	if(error==OK){
		system("clear");
		reservedSpace=strlen(text[0]);
		for(int i=0;i<rows;i++){
			strlenText=strlen(text[i]);
			if(strlenText>reservedSpace)
				reservedSpace=strlenText;
		}
		strlenText=strlen(title);
		if(strlenText>reservedSpace)
			reservedSpace=strlenText;


		printf("┏");
		for(int i=0;i<paddingLeft+reservedSpace+paddingRight;i++)
			printf("━");
		printf("┓\n");

		drawEmptyRow(paddingLeft,paddingRight,reservedSpace);
		drawRow(paddingLeft,paddingRight,reservedSpace,0,title);
		drawEmptyRow(paddingLeft,paddingRight,reservedSpace);

		for(int i=0;i<paddingUp;i++)
			drawEmptyRow(paddingLeft,paddingRight,reservedSpace);
		for(int i=0;i<rows;i++)
			drawRow(paddingLeft,paddingRight,reservedSpace,tabChoice[i],text[i]);
		drawRow(paddingLeft,paddingRight,reservedSpace,'Q',"Quitter");
		for(int i=0;i<paddingBottom;i++)
			drawEmptyRow(paddingLeft,paddingRight,reservedSpace);

		printf("┗");
		for(int i=0;i<paddingLeft+reservedSpace+paddingRight;i++)
			printf("━");
		printf("┛\n");
	}

	return error;
}



/**
 * @brief      Dessine une ligne vide du menu
 *
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 * @param[in]  reservedSpace  Espace réservé
 */
void drawEmptyRow(int paddingLeft,int paddingRight,int reservedSpace){
	printf("┃");
	for(int i=0;i<paddingLeft+reservedSpace+paddingRight;i++)
		printf(" ");
	printf("┃\n");
}



/**
 * @brief      Dessine une colonne remplie
 *
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 * @param[in]  reservedSpace  Espace réservé
 * @param[in]  choice         La lettre du choix
 * @param      title          Le titre
 */
void drawRow(int paddingLeft,int paddingRight,int reservedSpace, char choice, const char *title){
	int buff;
	int space = 0;
	printf("┃");
	for(int i=0;i<paddingLeft;i++){
		printf(" ");
	}

	if(choice!=0){
		printf("%c : %s",choice,title);
		buff=4;
	}
	else{
		printf("%s",title);
		buff=0;
	}

	space = reservedSpace-buff-strlen(title)+paddingRight;
	if(space < 0)
		space = 0;

	for(int i=0;i<space;i++)
		printf(" ");
	
	printf("┃\n");
}



/**
 * @brief      Dessine le menu
 *
 * @param      items	  Tableau des items à afficher  
 * @param[in]  rows       Le nombre de lignes
 * @param[in]  title      Le titre
 *
 * @return     L'erreur
 */
error_t menuCustom(const char items[][MAX_STRING_SIZE], int rows, const char * title){
	return menuCreate(rows,items,title,2,2,5,5);
}


/**
 * @brief      Détécte le choix de l'utilisateur
 *
 * @param      items	  Tableau des items à afficher  
 * @param[in]  rows       Le nombre de lignes
 * @param[in]  title      Le titre
 *
 * @return     Le choix
 */
char menuManagement(const char items[][MAX_STRING_SIZE], int rows, const char * title){
	char choix;
	menuCustom(items,rows,title);
	printf("Choix : ");
	choix=keyDetection();
	printf("\n\n");
	charUpper(&choix);
	while(!(choix>='1' && choix<='1'+rows) && choix!='Q'){
		printf(RED " Error\n" RESET);
		usleep(ERROR_SLEEP);
		menuCustom(items,rows,title);
		printf("Choix : ");
		choix=keyDetection();
		printf("\n\n");
		charUpper(&choix);
	}
	return choix;
}




/**
 * @brief      Detecte les touches sans que l'utilisateur n'appuie sur entré
 *
 * @return     Le caractère enfoncé
 */
char keyDetection(void){
	char c;   
	static struct termios oldt, newt;
	tcgetattr( STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON);          
	tcsetattr( STDIN_FILENO, TCSANOW, &newt);
	c=getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
	return c;
}
