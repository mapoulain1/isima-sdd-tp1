/**
 * @brief      Implémentation du type liste
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "liste.h"

#include <stdio.h>
#include <stdlib.h>
#include "production.h"

/**
 * @brief      Alloue un maillon
 *
 * @param      out      Le maillon
 * @param[in]  cost     Le coût
 * @param[in]  period   La période
 * @param[in]  factory  L'usine
 *
 * @return     L'erreur
 */
error_t nodeInit(node_t **out, float cost, int period, int factory){
	error_t error = OK;
	if(out == NULL){
		error = NULL_POINTER;
	}
	else{
		*out=(node_t*)malloc(sizeof(node_t));
		if(out!=NULL){
			(*out)->data.cost=cost;
			(*out)->data.period=period;
			(*out)->data.factory=factory;
		}
		else{
			error=ALLOC_ERROR;
			out=NULL;
		}
	}
	return error;
}

/**
 * @brief      Libère un maillon
 *
 * @param      node   Le maillon
 *
 * @return     L'erreur
 */
error_t nodeFree(node_t *node){
	if(node != NULL){
		free(node);
	}
	return OK;
}

/**
 * @brief      Initialise une liste chainée
 *
 * @return     La liste
 */
list_t listInit(void){
	return NULL;
}


/**
 * @brief      Exécute func sur chaque élément de la liste
 *
 * @param[in]  list  La liste
 * @param[in]  func  La fonction à exécuter
 *
 * @return     { description_of_the_return_value }
 */
list_t listForeach(list_t list, void(*func)(production_t)){
	node_t *tmp=list;
	while(tmp!=NULL){
		func(tmp->data);
		tmp=tmp->next;
	}
	return OK;
}


/**
 * @brief      Calcule la taille de la liste
 *
 * @param[in]  list  La liste
 *
 * @return     La taille de la liste
 */
int listLenght(list_t list){
	node_t *tmp=list;
	int size=0;
	while(tmp!=NULL){
		size++;
		tmp=tmp->next;
	}
	return size;
}


/**
 * @brief      Libère une liste chainée
 *
 * @param      list  La liste à libérer
 *
 * @return     L'erreur
 */
error_t listFree(list_t * list){
	while(*list!=NULL){
		listRemove(list);
	}
	*list = listInit();
	return OK;
}

/**
 * @brief      Affiche une liste chainée
 *
 * @param[in]  list  La liste à afficher
 *
 * @return     L'erreur
 */
error_t listPrint(list_t list){
	printf("Liste de production : \n");
	printf("┏━━━━━━━━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━━┓\n");
	printf("┃    Cout     ┃    Usine    ┃   Période   ┃\n");
	printf("┣━━━━━━━━━━━━━╋━━━━━━━━━━━━━╋━━━━━━━━━━━━━┫\n");
	listForeach(list,productionPrintf);
	printf("┗━━━━━━━━━━━━━┻━━━━━━━━━━━━━┻━━━━━━━━━━━━━┛\n");
	return OK;
}

/**
 * @brief      Ajoute une valeur dans une liste chainée
 *
 * @param      list  La liste
 * @param[in]  data  La valeur à ajouter
 *
 * @return     L'erreur
 */
error_t listAdd(list_t *prec, production_t data){
	error_t error;
	node_t *tmp = NULL;

	error = nodeInit(&tmp, data.cost, data.period, data.factory);
	if(error == OK){
		listAddChain(prec, tmp);
	}
	return error;
}


/**
 * @brief      Refait le chainage pour ajouter un maillon (adj_cell)
 *
 * @param      prec     Le précedent
 * @param      maillon  Le maillon à jouter
 *
 * @return     L'erreur
 */
error_t listAddChain(list_t *prec, node_t *maillon){
	maillon->next=*prec;
	*prec=maillon;
	return OK;
}

/**
 * @brief      Recherche une valeur dans une liste chainée trié
 *
 * @param      prec  Pointeur sur l'élément précédant celui à ajouter
 * @param[in]  data  La valeur à chercher
 *
 * @return     L'erreur
 */
error_t listSearchSorted(list_t *list, production_t data, node_t*** prec){
	*prec = list;
	node_t *tmp = *list;
	error_t err = OK;
	while(tmp!=NULL && tmp->data.cost > data.cost){
		*prec=&(tmp->next);
		tmp=tmp->next;
	}
	return err;
}

/**
 * @brief      Recherche une usine u à supprimer dans une liste chainée trié
 *
 * @param      prec  Pointeur sur l'élément précédant celui à supprimer
 * @param[in]  factory  L'usine à chercher
 *
 * @return     L'erreur
 */
error_t listSearchFactory(list_t *list, int factory, node_t*** prec) {
	*prec = list;
	node_t* tmp = *list;
	error_t err = OK;
	while ((tmp != NULL) && (tmp->data).factory != factory) {
		*prec = &(tmp->next);
		tmp = tmp->next;
	}
	return err;
}

/**
 * @brief      Supprime un élément (sup_cell)
 *
 * @param      node_t  Le pointeur sur le maillon
 *
 * @return     L'erreur
 */
error_t listRemove(node_t** prec){
	node_t *toFree = *prec;
	*prec=(*prec)->next;
	nodeFree(toFree);
	return OK;
}
