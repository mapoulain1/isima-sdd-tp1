/**
 * @brief      Déclaration des fonctions de gestion du menu
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef MENU
#define MENU


#include "menu.h"
#include "error.h"
#include "string.h"

/**
 * @brief      Fonction pour générer un menu dynamique
 *
 * @param[in]  rows           Le nombre de lignes
 * @param      text           Le tableau des items à afficher
 * @param      title          Le titre
 * @param[in]  paddingUp      Remplissage haut
 * @param[in]  paddingBottom  Remplissage bas
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 *
 * @return     L'erreur
 */
error_t menuCreate(int rows,const  char text[][MAX_STRING_SIZE],const char *title,int paddingUp,int paddingBottom,int paddingLeft,int paddingRight);

/**
 * @brief      Dessine une ligne vide du menu
 *
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 * @param[in]  reservedSpace  Espace réservé
 */
void drawEmptyRow(int paddingLeft,int paddingRight,int reservedSpace);

/**
 * @brief      Dessine une colonne remplie
 *
 * @param[in]  paddingLeft    Remplissage gauche
 * @param[in]  paddingRight   Remplissage droit
 * @param[in]  reservedSpace  Espace réservé
 * @param[in]  choice         Le choix
 * @param      title          Le titre
 */
void drawRow(int paddingLeft,int paddingRight,int reservedSpace, char choice, const char *title);


/**
 * @brief      Dessine le menu
 *
 * @param      items	  Tableau des items à afficher  
 * @param[in]  rows       Le nombre de lignes
 * @param[in]  title      Le titre
 *
 * @return     L'erreur
 */
error_t menuCustom(const char items[][MAX_STRING_SIZE], int rows, const char * title);


/**
 * @brief      Détécte le choix de l'utilisateur
 *
 * @return     Le choix
 */
char menuChoice(void);


/**
 * @brief      Détécte le choix de l'utilisateur
 *
 * @param      items	  Tableau des items à afficher  
 * @param[in]  rows       Le nombre de lignes
 * @param[in]  title      Le titre
 *
 * @return     Le choix
 */
char menuManagement(const char items[][MAX_STRING_SIZE], int rows, const char * title);

/**
 * @brief      Detecte les touches sans que l'utilisateur n'appuie sur entré
 *
 * @return     Le caractère enfoncé
 */
char keyDetection(void);

#endif
