/**
 * @brief      Déclaration de la gestion de la sauvegarde des fichiers
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef GESTION_FICHIER
#define GESTION_FICHIER

#include "error.h"
#include "liste.h"
#include "bool.h"

/**
 * @brief      Sauvegarde une liste chainée dans un fichier text
 *
 * @param[in]  list      La liste à sauvegarder
 * @param[in]  filename  Le nom du fichier

 *
 * @return     L'erreur
 */
error_t listSave(list_t list, const char * filename);

/**
 * @brief      Crée dynamiquement le nom du fichier qui doit stocker la sauvegarde à partir du nom du fichier original de la matrice
 *
 * @param[in]  filename     Le nom du fichier original
 * @param[in]  output_name  Le nom du fichier de sauvegarde

 *
 * @return     L'erreur
 */
error_t saveFilenameConstruct(const char* filename, char* output_name, bool timestamp);

#endif