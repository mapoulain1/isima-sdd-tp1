/**
 * @brief      Implémentation de la gestion du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "gestion.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "matrix.h"
#include "error.h"
#include "liste.h"
#include "menu.h"
#include "gestion.h"
#include "color.h"
#include "gestionListeProduction.h"
#include "string.h"
#include "gestionFichier.h"

/**
 * @brief      Permet d'avoir l'interface graphique de l'application
 *
 * @return     L'erreur
 */
error_t gestionGraphique(void){
	error_t error = OK;
	list_t list=listInit();
	matrix_t matrix=matrixZero();

	char items[5][MAX_STRING_SIZE]={"Charger un fichier", "Garder K valeurs", "Afficher la liste","Supprimer une usine", "Sauvegarder la liste"};
	char title[MAX_STRING_SIZE]= "Gestion des couts de productions";
	char choice;
	char filename[MAX_STRING_SIZE];
	char saveFilename[MAX_STRING_SIZE];


	char kvalue_s[MAX_STRING_SIZE];
	int kvalue = UNDEFINED;
	
	char factory_s[MAX_STRING_SIZE];
	int factory = UNDEFINED;

	bool estCharge = false;
	bool estCreeeListe = false;

	choice = menuManagement(items,sizeof(items)/MAX_STRING_SIZE,title);
	while(choice != 'Q'){
		switch(choice){
			case '1':
			
			printf("Nom du fichier à charger : ");
			fgets(filename,MAX_STRING_SIZE,stdin);
			filename[strlen(filename)-1]='\0';
			printf("\n");

			error = matrixCreate(filename, &matrix);
			if(error != OK){
				logError(error);
			}
			else{
				estCharge = true;
				printf(GRE "Matrice chargée avec succès !\n" RESET);
			}
			break;


			case '2':
			if (!estCharge) {
				printf(RED "ERREUR : Aucun fichier n'est actuellement chargé\n" RESET);
			}
			else {
				printf("Nombre de valeurs à garder (-1 pour tout charger) : ");
				fgets(kvalue_s,MAX_STRING_SIZE,stdin);
				kvalue_s[strlen(kvalue_s)-1]='\0';
				kvalue = atoi(kvalue_s);
				printf("\n");
				if(kvalue < 0){
					kvalue=matrix.rows * matrix.cols;
				}
				error = listKeepKValues(matrix, &list, kvalue);
				if(error != OK){
					logError(error);
				}
				else{
					estCreeeListe = true;
					printf(GRE "Valeurs conservées avec succès !\n" RESET);
				}
			}
			break;


			case '3':
			if (!estCreeeListe) {
				printf(RED "ERREUR : Vous devez charger K valeurs avant d'afficher la liste\n" RESET);
			}
			else {
				listPrint(list);
			}
			break;



			case '4':
			if (!estCreeeListe) {
				printf(RED "ERREUR : Vous devez charger K valeurs avant de supprimer une usine de la liste\n" RESET);
			}
			else {
				printf("Numéro de l'usine à supprimer : ");
				fgets(factory_s,MAX_STRING_SIZE,stdin);
				factory = strtol(factory_s, (char**)NULL, 10);
				printf("\n");

				error = listDeleteFactory(&list, factory);
				if(error != OK){
					logError(error);
				}
				else{
					estCreeeListe = true;
					printf(GRE "Usine supprimée avec succès !\n" RESET);
				}
			}
			break;

			case '5':
			if (estCreeeListe) {
				saveFilenameConstruct(filename, saveFilename, true);
				listSave(list, saveFilename);
				printf(GRE "Liste sauvegardée sous le nom : \"%s\"!\n" RESET,saveFilename);
			}
			else{
				printf(RED "ERREUR : Aucune liste à sauvegarder\n" RESET);
			}
			break;
		}

		getchar();
		choice=menuManagement(items,sizeof(items)/MAX_STRING_SIZE,title);
	}

	

	listFree(&list);
	matrixFree(&matrix);
	return error;
}


/**
 * @brief      Permet d'avoir la gestion en ligne de commande
 *
 * @param[in]  argc  Le nombre d'arguents
 * @param      argv  Le tableau des arguments
 *
 * @return     L'erreur
 */
error_t gestionArgs(const char * filename, int kvalue, int factory, bool saveFile){
	error_t error = OK;
	list_t list=listInit();
	matrix_t matrix=matrixZero();
	char saveFilename[MAX_STRING_SIZE];


	error = matrixCreate(filename, &matrix);
	if(error != OK){
		logError(error);
	}
	else{
		if(kvalue == UNDEFINED){
			kvalue=matrix.rows * matrix.cols;
		}
		listKeepKValues(matrix, &list, kvalue);
		if(factory != UNDEFINED){
			listDeleteFactory(&list, factory);
		}
		listPrint(list);

		if(saveFile){
			saveFilenameConstruct(filename, saveFilename, false);
			listSave(list, saveFilename);
		}
	}
	listFree(&list);
	matrixFree(&matrix);

	return error;
}

/**
 * @brief      Affiche l'aide pour l'utilisation du programme
 *
 * @return     L'erreur
 */
error_t gestionHelp(void){
	printf(RED "\nHELP gestion_production : \n\n\n" RESET);

	printf("\tUSAGE : \n");
	printf("\t./gestion_production [OPTION] [FILE] [K Valeurs] [Usine]\n");
	printf("\tOPTION : \n");
	printf("\t\t-g : interface graphique\n");
	printf("\t\t-h : aide\n");
	printf("\t\n");
	printf("\tFILE : \n");
	printf("\t\tFichier texte avec une matrice représenté comme ceci : \n");
	printf("\t\t\tn m\n");
	printf("\t\t\tx x x x x x\n");
	printf("\t\t\tx x x x x x\n");
	printf("\t\t\tx x x x x x\n");
	printf("\t\t\tx x x x x x\n");
	printf("\t\n");
	printf("\tK Valeurs\n");
	printf("\t\tNombre de couts de productions les plus faibles à garder\n");
	printf("\t\tNe pas utiliser avec -g\n\n");
	printf("\tUsine\n");
	printf("\t\tUsine à supprimer de la liste\n");
	printf("\t\tNe pas utiliser avec -g\n\n");
	printf("\tExemples :\n");
	printf("\t\t./gestion_production -g\n");
	printf("\t\t./gestion_production production.dat 50\n");
	printf("\t\t./gestion_production production.dat 50 2\n\n");

	printf(GRE "\tCOPYRIGHT - Lilian HERTEL - Maxime POULAIN\n\n\n" RESET);

	return OK;
}