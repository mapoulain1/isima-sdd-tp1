/**
 * @brief      Point d'entrée du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "matrix.h"
#include "error.h"
#include "liste.h"
#include "menu.h"
#include "gestion.h"
#include "gestionListeProduction.h"
#include "color.h"


#define UNDEFINED -1

/**
 * @brief      Point d'entrée du programme
 *
 * @param[in]  argc  Le nombre d'argument
 * @param      argv  Le tableau contenant les arguments
 *
 * @return     Code d'erreur du programme
 */
int main(int argc, char const *argv[]){

	char const * filename = NULL;
	int kvalue = UNDEFINED;
	int factory = UNDEFINED;

	if(argc <= 1){
		gestionHelp();
	}
	else{
		if(strcmp(argv[1], "-g")==0){
			gestionGraphique();
		}
		else if(strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0){
			gestionHelp();
		}
		else{
			if(argc >= 2){
				filename=argv[1];
			}
			if(argc >= 3){
				kvalue=atoi(argv[2]);
			}
			if(argc >= 4){
				factory=atoi(argv[3]);
			}
			gestionArgs(filename, kvalue, factory, true);
		}
	}
	return 0;
}