/**
 * @brief      Définition du type liste
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef LISTE
#define LISTE

#include "production.h"
#include "error.h"
#include "matrix.h"


/**
 * Structure représentant une liste chainée
 */
typedef struct node{
	struct node *next;
	production_t data;
} node_t, *list_t;


/*
 *
 * @param      out      Le maillon
 * @param[in]  cost     Le coût
 * @param[in]  period   La période
 * @param[in]  factory  L'usine
 *
 * @return     L'erreur
 */
error_t nodeInit(node_t **out, float cost, int period, int factory);


/**
 * @brief      Libère un maillon
 *
 * @param      node   Le maillon
 *
 * @return     L'erreur
 */
error_t nodeFree(node_t *node);

/**
 * @brief      Initialise une liste chainée
 *             
 * @return     La liste
 */
list_t listInit(void);

/**
 * @brief      Exécute func sur chaque élément de la liste
 *
 * @param[in]  list  La liste
 * @param[in]  func  La fonction à exécuter
 *
 * @return     { description_of_the_return_value }
 */
list_t listForeach(list_t list, void(*func)(production_t));

/**
 * @brief      Calcule la taille de la liste
 *
 * @param[in]  list  La liste
 *
 * @return     La taille de la liste
 */
int listLenght(list_t list);

/**
 * @brief      Libère une liste chainée
 *
 * @param      list  La liste à libérer
 *
 * @return     L'erreur
 */
error_t listFree(list_t * list);

/**
 * @brief      Affiche une liste chainée
 *
 * @param[in]  list  La liste à afficher
 *
 * @return     L'erreur
 */
error_t listPrint(list_t list);

/**
 * @brief      Ajoute une valeur dans une liste chainée
 *
 * @param      prec  Pointeur sur l'élément précédant celui à ajouter
 * @param[in]  data  La valeur à ajouter
 *
 * @return     L'erreur
 */
error_t listAdd(list_t *prec, production_t data);

/**
 * @brief      Refait le chainage pour ajouter un maillon (adj_cell)
 *
 * @param      prec     Le précedent
 * @param      maillon  Le maillon à jouter
 *
 * @return     L'erreur
 */
error_t listAddChain(list_t *prec, node_t *maillon);

/**
 * @brief      Recherche une valeur dans une liste chainée trié
 *
 * @param      prec  Pointeur sur l'élément précédant celui à ajouter
 * @param[in]  data  La valeur à chercher
 *
 * @return     L'erreur
 */
error_t listSearchSorted(list_t *list, production_t data, node_t*** prec);

/**
 * @brief      Recherche une usine u à supprimer dans une liste chainée trié
 *
 * @param      prec  Pointeur sur l'élément précédant celui à supprimer
 * @param[in]  factory  L'usine à chercher
 *
 * @return     L'erreur
 */
error_t listSearchFactory(list_t *list, int factory, node_t*** prec);

/**
 * @brief      Supprime un élément (sup_cell)
 *
 * @param      node_t  Le pointeur sur le maillon
 *
 * @return     L'erreur
 */
error_t listRemove(node_t** prec);

#endif