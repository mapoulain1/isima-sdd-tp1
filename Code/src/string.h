/**
 * @brief      Définition des fonctions de traitement de chaines de caractères
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef STRING
#define STRING

#define MAX_STRING_SIZE 512

/**
 * @brief      Met en majuscule une chaine de caractère
 *
 * @param      chaine  La chaine à mettre en majuscule
 */
void stringUpper(char* chaine);


/**
 * @brief      Met en majuscule un caractère
 *
 * @param      chaine  Le caractère
 */
void charUpper(char* c);


#endif