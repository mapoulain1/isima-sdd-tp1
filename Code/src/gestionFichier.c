/**
 * @brief      Implémentation de la gestion de la sauvegarde des fichiers
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "gestionFichier.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "error.h"
#include "liste.h"

/**
 * @brief      Sauvegarde une liste chainée dans un fichier text
 *
 * @param[in]  list      La liste à sauvegarder
 * @param[in]  filename  Le nom du fichier
 *
 * @return     L'erreur
 */
error_t listSave(list_t list, const char * filename){
	node_t *tmp=list;
	error_t error = OK;
	FILE * flot;
	flot=fopen(filename, "w");
	if(flot!=NULL){
		fprintf(flot, "Liste de production : \n");
		fprintf(flot, "┏━━━━━━━━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━━┓\n");
		fprintf(flot, "┃    Cout     ┃    Usine    ┃   Période   ┃\n");
		fprintf(flot, "┣━━━━━━━━━━━━━╋━━━━━━━━━━━━━╋━━━━━━━━━━━━━┫\n");
		while(tmp!=NULL){
			productionPrint(flot,tmp->data);
			tmp=tmp->next;
		}	
		fprintf(flot, "┗━━━━━━━━━━━━━┻━━━━━━━━━━━━━┻━━━━━━━━━━━━━┛\n");
		fclose(flot);
	}
	else{
		error=FILE_NOT_FOUND;
	}
	return error;
}

/**
 * @brief      Crée dynamiquement le nom du fichier qui doit stocker la sauvegarde à partir du nom du fichier original de la matrice
 *
 * @param[in]  filename     Le nom du fichier original
 * @param[in]  output_name  Le nom du fichier de sauvegarde

 *
 * @return     L'erreur
 */
error_t saveFilenameConstruct(const char* filename, char* output_name, bool timestamp) {
	error_t error = OK;
	char * ext;
	int sizeFilenameWithoutExt = 0;
	time_t t;
	struct tm tm;


	if(filename==NULL){
		sprintf(output_name,"unknown.save");
	}
	else{
		ext = strrchr(filename, '.');
		sizeFilenameWithoutExt = (ext == NULL) ? strlen(filename) : (long unsigned int)(ext-filename);

		if(timestamp){
			t = time(NULL);
			tm = *localtime(&t);
			sprintf(output_name,"%.*s_%02d:%02d:%02d.save",sizeFilenameWithoutExt,filename,tm.tm_hour, tm.tm_min, tm.tm_sec);
		}
		else{
			sprintf(output_name,"%.*s.save",sizeFilenameWithoutExt,filename);
		}
	}

	return error;
}