/**
 * @brief      Implémentation de la gestion spécifique de la liste
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */


#include <stdio.h>
#include "liste.h"
#include "production.h"



/**
 * @brief      Charge K valeurs de couts les plus faible dans une liste chainée
 *
 * @param[in]  m     La matrice contenant les valeurs
 * @param      list  La liste
 * @param[in]  k     Le nombre K de valeur à garder
 *
 * @return     L'erreur
 */
error_t listKeepKValues(matrix_t m, list_t *list, int k) {
    error_t error = OK;
    production_t currData;
    node_t** prec;
    int i, j, cpt = 0;

    for (i = 0; i < m.rows; i++) {
        for (j = 0; j < m.cols; j++) {
            currData.cost = m.matrix[i][j];
            currData.factory = i;
            currData.period = j;
            

            if(cpt<k){
                listSearchSorted(list, currData, &prec);
                listAdd(prec, currData);   
            }
            else if(*list != NULL && currData.cost < (*list)->data.cost){
                listRemove(list);
                listSearchSorted(list, currData, &prec);
                listAdd(prec, currData); 
            }
            cpt++;
        }
    }
    return error;
}

/**
 * @brief      Supprime toutes les occurences d'une usine dans la liste chainée
 *             
 * @param      list     La liste
 * @param[in]  factory  L'usine à supprimer
 *
 * @return     L'erreur
 */
error_t listDeleteFactory(list_t *list, int factory) {
    node_t** prec = NULL;
    error_t error = OK;
    error = listSearchFactory(list, factory, &prec);
    while(*prec != NULL) {
        listRemove(prec);
        error = listSearchFactory(list, factory, &prec);
    }
    return error;
}

