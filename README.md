# Lilian HERTEL - Maxime POULAIN - Structure de données - TP1

TP1 de Structure de données pour le binome Lilian HERTEL - Maxime POULAIN.

## Usage 
```
cd Code
make
./gestion_production -g 
./gestion_production matrices/dataNormal.txt 10
./gestion_production matrices/dataNormal.txt 10 0 
```

## Documentation

[Dossier explicatif (Documentation/Dossier HERTEL POULAIN - TP1.pdf)](Documentation/Dossier HERTEL POULAIN - TP1.pdf)

[Documentation du code généré automatiquement (Documentation/Doxygen/Generated/html/index.html)](Documentation/Doxygen/Generated/html/index.html)

